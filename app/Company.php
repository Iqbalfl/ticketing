<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'user_id'
    ];

    protected $appends = ['display_status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Menunggu Verifikasi';
                break;

            case '200':
                $result = 'Terverifikasi';
                break;
            case '10':
                $result = 'Tidak Aktif';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
