<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $appends = ['display_status'];

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'employee_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Menunggu Persetujuan';
                break;
            case '200':
                $result = 'Telah Berkontrak';
                break;
            case '300':
                $result = 'Kontrak Selesai';
                break;
            case '10':
                $result = 'Dibatalkan';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
