<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AlternativeCriteria;

class Employee extends Model
{
    protected $appends = ['display_status', 'display_rating', 'description'];
    
    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];
        $is_employed = $this->is_employed;

        switch ($status) {
            case '100':
                $result = 'Aktif' . ($is_employed == 1 ? " (Sedang Berkontrak)" : " (Tersedia)");
                break;
            case '10':
                $result = 'Tidak Aktif';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplayRatingAttribute()
    {
        $rating = $this->attributes['rating'];

        return star_rating($rating);
    }

    public function getDescriptionAttribute()
    {
        $result = "";

        $criteria = AlternativeCriteria::where('alternative_id', $this->id)->with('criteria')->get();
        foreach ($criteria as $key => $item) {
            $result .= $item->criteria->name . " : <b>" . $item->score . "</b><br>";
        }

        return $result;
    }
}
