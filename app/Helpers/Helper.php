<?php 

use Carbon\Carbon;

function setActive($uri, $output = 'active')
{
	if( is_array($uri) ) {
		foreach ($uri as $u) {
			if (Route::is($u)) {
				return $output;
			}
		}
	} else {
		if (Route::is($uri)){
			return $output;
		}
	}
}

function rupiah($number)
{
	return number_format($number,0,',','.');
}

function date_full($date)
{
	return Carbon::parse($date)->format('d F Y');
}

function date_dmy($date)
{
	return Carbon::parse($date)->format('d M Y');
}

function diff_days($start_date, $end_date)
{
	$start = Carbon::parse($start_date);
	$res = Carbon::parse($end_date)->diffInDays($start);

	return $res;
}

function nilai_text($nilai)
{
	$text = "";

	if ($nilai >= 90 && $nilai <= 100) {
		$text = "Sangat Bagus";
	} else if ($nilai >= 80 && $nilai <= 89.9){
		$text = "Bagus";
	} else if ($nilai >= 70 && $nilai <= 79.9){
		$text = "Cukup Bagus";
	} else if ($nilai >= 60 && $nilai <= 69.9){
		$text = "Tidak Terlalu Bagus";
	} else if ($nilai <= 59) {
		$text = "Tidak Bagus";
	}

	return $text;
}

function decimal_fixed($num){
	return number_format((float)$num, 3, '.', '');
}

function star_rating($rate)
{
	$res = "";
	for ($i=0; $i < ceil($rate); $i++) { 
		$class = "fa-star";
		if ($i+1 == ceil($rate) && ($rate > floor($rate))){
			$class = "fa-star-half";
		}

		$res .= '<i class="fas '.$class.' star-rate"></i>';
	}

	return ($rate > 0 ? $res : "-");
}

function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}