<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use Session;
use App\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $orders = Order::with('user', 'ticket');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $orders->whereStatus($request->status);
                }
            }

            $orders = $orders->select('orders.*');

            return Datatables::of($orders)
                ->addIndexColumn()
                ->addColumn('action', function ($order) {
                    return view('partials._action', [
                        'model'           => $order,
                        // 'form_url'        => route('order.destroy', $order->id),
                        'edit_url'        => route('order.edit', $order->id),
                        'show_url'        => route('order.show', $order->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        return view('backend.orders.show')->with(compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);

        return view('backend.orders.edit')->with(compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required|integer',
        ]);

        $order = Order::find($id);
        $order->status = $request->status;
        $order->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengubah order"
        ]);

        return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
