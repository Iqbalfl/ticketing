<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use Session;
use App\Payment;
use App\Order;


class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $payments = Payment::with('user', 'order');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $payments->whereStatus($request->status);
                }
            }

            $payments = $payments->select('payments.*');

            return Datatables::of($payments)
                ->addIndexColumn()
                ->addColumn('action', function ($payment) {
                    return view('partials._action', [
                        'model'           => $payment,
                        // 'form_url'        => route('payment.destroy', $payment->id),
                        // 'edit_url'        => route('payment.edit', $payment->id),
                        'verif_url'        => route('payment.edit', $payment->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = Payment::find($id);

        return view('backend.payments.edit')->with(compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'is_paid' => 'required|integer',
        ]);

        $payment = Payment::find($id);
        $payment->is_paid = $request->is_paid;
        $payment->save();

        $order = Order::find($payment->order_id);
        $order->status = 200;
        $order->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil melakukan verifikasi"
        ]);

        return redirect()->route('payment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
