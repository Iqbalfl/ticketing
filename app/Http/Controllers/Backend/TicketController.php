<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use Session;
use App\Order;
use App\Ticket;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $tickets = Ticket::query();

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $tickets->whereStatus($request->status);
                }
            }

            $tickets = $tickets->select('tickets.*');

            return Datatables::of($tickets)
                ->addIndexColumn()
                ->addColumn('action', function ($ticket) {
                    return view('partials._action', [
                        'model'           => $ticket,
                        'form_url'        => route('ticket.destroy', $ticket->id),
                        'edit_url'        => route('ticket.edit', $ticket->id),
                        'show_url'        => route('ticket.show', $ticket->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.tickets.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'string',
            'event_date' => 'required|date',
            'price' => 'required|integer',
            'image' => 'required|image',
            'video_url' => 'string',
        ]);

        $ticket = new Ticket();
        $ticket->name = $request->name;
        $ticket->description = $request->description;
        $ticket->event_date = $request->event_date;
        $ticket->price = $request->price;
        $ticket->video_url = $request->video_url;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tickets';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
           
            $ticket->image = $filename;
        }
        $ticket->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Tiket dengtan nama $ticket->name berhasil dibuat"
        ]);

        return redirect()->route('ticket.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::find($id);

        return view('backend.tickets.show')->with(compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::find($id);

        return view('backend.tickets.edit')->with(compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'string',
            'event_date' => 'required|date',
            'price' => 'required|integer',
            'image' => 'image',
            'video_url' => 'string',
        ]);

        $ticket = Ticket::find($id);
        $ticket->name = $request->name;
        $ticket->description = $request->description;
        $ticket->event_date = $request->event_date;
        $ticket->price = $request->price;
        $ticket->video_url = $request->video_url;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tickets';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // delete old image
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tickets'
                . DIRECTORY_SEPARATOR . $ticket->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
           
            $ticket->image = $filename;
        }
        $ticket->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Tiket dengtan nama $ticket->name berhasil diubah"
        ]);

        return redirect()->route('ticket.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);

        $check = Order::where('ticket_id', $id)->count();

        if ($check > 0) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Tidak dapat menghapus tiket ini, karena tiket ini sudah memiliki order. Alternatif anda dapat menonaktifkannya"
            ]);
            return redirect()->back();
        }

        if ($ticket->image) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tickets'
                . DIRECTORY_SEPARATOR . $ticket->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        $ticket->delete();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil menghapus data"
        ]);

        return redirect()->route('ticket.index');
    }
}
