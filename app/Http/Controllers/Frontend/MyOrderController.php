<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use Session;
use App\Order;
use App\Payment;

class MyOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $orders = Order::with('user', 'ticket')->where('user_id', auth()->user()->id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $orders->whereStatus($request->status);
                }
            }

            $orders = $orders->select('orders.*');

            return Datatables::of($orders)
                ->addIndexColumn()
                ->addColumn('action', function ($order) {
                    return view('partials._action', [
                        'model'           => $order,
                        // 'form_url'        => route('order.destroy', $order->id),
                        // 'edit_url'        => route('order.edit', $order->id),
                        'show_url'        => route('my-order.show', $order->id),
                        'confirm_url'        => route('my-order.edit', $order->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.my-orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        return view('frontend.my-orders.show')->with(compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);

        return view('frontend.my-orders.edit')->with(compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'account_holder' => 'required|string|max:255',
            'total_price' => 'required|integer',
            'image' => 'required|image',
        ]);

        $payment = new Payment();
        $payment->order_id = $id;
        $payment->user_id = auth()->user()->id;
        $payment->total_price = $request->total_price;
        $payment->method = 'Transfer Bank BCA';
        $payment->account_holder = $request->account_holder;
        $payment->is_paid = 0;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'payments';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // delete old image
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'payments'
                . DIRECTORY_SEPARATOR . $payment->image;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
           
            $payment->image = $filename;
        }
        $payment->save();

        $order = Order::find($id);
        $order->status = 150;
        $order->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil melakukan konfirmasi pembayaran!"
        ]);

        return redirect()->route('my-order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
