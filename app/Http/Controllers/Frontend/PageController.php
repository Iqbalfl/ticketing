<?php

namespace App\Http\Controllers\Frontend;

use App\AlternativeCriteria;
use App\Contract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Member;
use App\Employee;
use App\Ticket;
use Auth;
use Session;

class PageController extends Controller
{
    public function landing(Request $request)
    {
        $tickets = Ticket::where('status', 100);

        $condition = (env('DB_CONNECTION', 'mysql') == 'pgsql') ? 'ilike' : 'like';

        if ($request->get('q') != null) {
            if ($request->q != 'all') {
                $tickets->where('name', $condition, '%' . $request->q . '%');
            }
        }

        $tickets = $tickets->paginate(8);
        $q = $request->q;

        return view('frontend.pages.landing')->with(compact('tickets', 'q'));
    }

    public function pageAbout()
    {
        return view('frontend.pages.about');
    }

    public function checkout($id)
    {
        $ticket = Ticket::find($id);
        $user = Auth::user();

        if (!$ticket){
            abort(404);
        }

        return view('frontend.pages.checkout')->with(compact('ticket', 'user'));
    }

    public function checkoutStore(Request $request)
    {
        $this->validate($request, [
            'ticket_id' => 'required|integer',
            'quantity' => 'required|integer',
            'total_price' => 'required|integer',
        ]);

        $id = $request->ticket_id;

        $order = new Order();
        $order->code = strtoupper(uniqid());
        $order->user_id = auth()->user()->id;
        $order->ticket_id = $id;
        $order->quantity = $request->quantity;
        $order->total_price = $request->total_price;
        $order->status = 100;
        $order->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Pemesanan tiket berhasil, silahkan lakukan pembayaran dan konfirmasi!"
        ]);

        return redirect()->route('my-order.edit', $order->id);
    }
}
