<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $auth = \Auth::user()->role;

        if ($auth == 'admin' || $auth == 'staff') {
            return $this->admin();
        } else {
            return $this->client();
        }
    }

    public function admin()
    {
        return view('backend.home');
    }

    public function client()
    {
        return view('frontend.home');
    }
}
