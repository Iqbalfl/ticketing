<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $appends = ['display_status', 'display_date'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->is_paid;

        switch ($status) {
            case 1:
                $result = 'Terverifikasi';
                break;
            case 0:
                $result = 'Menunggu Verifikasi';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplayDateAttribute()
    {
        $result = '';

        $raw = $this->created_at;
        if ($raw != null) {
            $result = date_dmy($raw);
        }
        
        return $result;
    }
}
