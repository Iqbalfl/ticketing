@extends('layouts.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">

    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Verifikasi Pembayaran</h6>
      </div>
      <div class="card-body">
        
        <div class="row">
          <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-header">
                Informasi Order - &nbsp <strong>{{ $payment->order->code }}</strong>
              </div>
              <div class="card-body">
                <div>
                  Nama Pembeli<br>
                  <strong>{{ $payment->order->user->name }}</strong>
                </div>
                <hr>
                <div>
                  Tiket <br>
                  <strong>{{ $payment->order->ticket->name }}</strong>
                </div>
                <hr>
                <div>
                  Tanggal Tiket <br>
                  <strong>{{ $payment->order->ticket->event_date }}</strong>
                </div>
                <hr>
                <div>
                  Status Order <br>
                  <span class="badge badge-primary">
                    {{ $payment->order->display_status }}
                  </span>
                </div>
              </div>

            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-primary">
              <div class="card-header">
                Informasi Harga
              </div>
              <div class="card-body">
                <div>
                  Harga Tiket<br>
                  <strong>Rp {{ rupiah($payment->order->ticket->price) }}</strong>
                </div>
                <hr>
                <div>
                  Qty<br>
                  <strong>Rp {{ rupiah($payment->order->quantity) }}</strong>
                </div>
                <hr>
                <div>
                  Total Harga <br>
                  <strong>Rp {{ rupiah($payment->order->total_price) }}</strong>
                </div>
                <hr>
                {{-- <div>
                  Cara Pembayaran <br>
                  <span class="badge badge-primary">
                    <strong>{{ $payment->order->payment_type_name }}</strong>
                  </span>
                </div> 
                <hr> --}}
              </div>
            </div>

          </div>
          <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-header">
                Informasi Pembayaran</strong>
              </div>
              <form method="POST" action="{{ route('payment.update', $payment->order->id) }}" class="card-body">
                @csrf
                @method('PUT')
                <div>
                  Rekening Atas Nama<br>
                  <strong>{{ $payment->account_holder }}</strong>
                </div>
                <hr>
                <div>
                  Jumlah Transfer <br>
                  <strong>{{ $payment->total_price }}</strong>
                </div>
                <hr>
                <div>
                  Tanggal Transfer <br>
                  <strong>{{ $payment->display_date }}</strong>
                </div>
                <hr>
                <div>
                  Status Pembayaran <br>
                  <span class="badge badge-primary">
                    {{ $payment->display_status }}
                  </span>
                </div>
                <hr>
                <div class="form-group {{ $errors->has('is_paid') ? ' has-error' : '' }}" style="display: none;">
                  <label for="is_paid">Verifikasi</label>
                  <select name="is_paid" class="form-control select2">
                    <option value="1">Verifikasi</option>
                  </select>
                  @if ($errors->has('is_paid'))
                    <div class="invalid-feedback">
                      {{ $errors->first('is_paid') }}
                    </div>
                  @endif
                </div>

                <div class="form-group mt-2">
                  <button type="submit" class="btn btn-success btn-block" tabindex="4">
                    Verifikasi
                  </button>
                </div>

              </form>

            </div>
          </div>

          <div class="col-lg-4">
            <div class="card card-primary">
              <div class="card-header py-3">
                <h6 class="m-0">Bukti Pembayaran</h6>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="text-center">
                    @if (is_null($payment->image))
                      <img src="{{ asset('assets/stisla/img/news/img02.jpg') }}" class="rounded" id="image-prev" width="200" alt="avatar">
                    @else
                      <img alt="image" src="{{asset('uploads/images/payments/'.$payment->image)}}" class="rounded" id="image-prev" width="200" alt="images">
                    @endif
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
            
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection