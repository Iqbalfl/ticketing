@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Tambah Tiket</h1>
    </div>

    <form method="POST" action="{{ route('ticket.store') }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Tiket</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Tiket</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ old('name') }}">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>
                <div class="form-group col-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description" class="d-block">Deskripsi</label>
                  <input id="description" type="text" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" value="{{ old('description') }}">
                  @if ($errors->has('description'))
                    <div class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('price') ? ' has-error' : '' }}">
                  <label for="price">Harga Tiket</label>
                  <input id="price" type="text" class="form-control @if ($errors->has('price')) is-invalid @endif" name="price" tabindex="1" value="{{ old('price') }}">
                  @if ($errors->has('price'))
                    <div class="invalid-feedback">
                      {{ $errors->first('price') }}
                    </div>
                  @endif
                </div>

                <div class="form-group col-12 {{ $errors->has('event_date') ? ' has-error' : '' }}">
                  <label for="event_date">Tanggal Tiket</label>
                  <input id="event_date" type="date" class="form-control @if ($errors->has('event_date')) is-invalid @endif" name="event_date" tabindex="1" value="{{ old('event_date') }}">
                  @if ($errors->has('event_date'))
                    <div class="invalid-feedback">
                      {{ $errors->first('event_date') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group {{ $errors->has('video_url') ? ' has-error' : '' }}">
                <label for="video_url" class="d-block">Youtube Video Url</label>
                <input id="video_url" type="text" class="form-control @if ($errors->has('video_url')) is-invalid @endif" name="video_url" value="{{ old('video_url') }}">
                @if ($errors->has('video_url'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('video_url') }}</strong>
                  </div>
                @endif
              </div>              

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Image / Marker</h6>
            </div>
            <div class="card-body">
              <div class="form-group">
                <div class="text-center">
                  <img src="{{ asset('assets/stisla/img/news/img02.jpg') }}" class="rounded" id="image-prev" width="200" alt="avatar">
                </div>
              </div>
              <div class="form-group custom-file mb-3 {{ $errors->has('image') ? ' has-error' : '' }}">
                <input id="image" type="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" name="image" />
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                @if ($errors->has('image'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('image') }}</strong>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#image-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#image").change(function() {
      readURL(this);
    });
  </script>
@endsection