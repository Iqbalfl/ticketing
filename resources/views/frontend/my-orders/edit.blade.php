@extends('layouts.front')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Upload Bukti Pembayaran</h1>
    </div>

    <form method="POST" action="{{ route('my-order.update', $order->id) }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-8">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Pembayaran</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}
              @method('PUT')

              <div class="alert alert-info alert-has-icon">
                <div class="alert-icon"><i class="fa fa-info"></i></div>
                <div class="alert-body">
                  <div class="alert-title">Transfer</div>
                  <span>Silahkan Transfer ke bank BCA</span> <br>
                  <span>Atas nama : <b>Museum Geologi Bandung </b></span> <br>
                  <span>No. Rek. : <b>900167187615 </b></span> <br>
                </div>
              </div>

              <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                <label for="code">Order Code / Invoice</label>
                <input id="code" type="text" class="form-control @if ($errors->has('code')) is-invalid @endif" name="code" tabindex="1" value="{{ $order->code }}" readonly>
                {{-- <input id="id" type="hidden" name="order_id" tabindex="1" value="{{ $order->id }}" readonly> --}}
                @if ($errors->has('code'))
                  <div class="invalid-feedback">
                    {{ $errors->first('code') }}
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('total_price_order') ? ' has-error' : '' }}">
                <label for="total_price_order">Total Order</label>
                <input id="total_price_order" type="text" class="form-control @if ($errors->has('total_price_order')) is-invalid @endif" name="total_price_order" tabindex="1" value="{{ $order->total_price }}" readonly>
                @if ($errors->has('total_price_order'))
                  <div class="invalid-feedback">
                    {{ $errors->first('total_price_order') }}
                  </div>
                @endif
              </div>

              <div class="row">
                <div class="form-group col-12 {{ $errors->has('account_holder') ? ' has-error' : '' }}">
                  <label for="account_holder">Transfer Atas Nama</label>
                  <input id="account_holder" type="text" class="form-control @if ($errors->has('account_holder')) is-invalid @endif" name="account_holder" tabindex="1" value="{{ old('account_holder') }}">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('account_holder') }}
                    </div>
                  @endif
                </div>
                <div class="form-group col-12 {{ $errors->has('total_price') ? ' has-error' : '' }}">
                  <label for="total_price">Jumlah Transfer</label>
                  <input id="total_price" type="text" class="form-control @if ($errors->has('total_price')) is-invalid @endif" name="total_price" tabindex="1" value="{{ $order->total_price }}">
                  @if ($errors->has('total_price'))
                    <div class="invalid-feedback">
                      {{ $errors->first('total_price') }}
                    </div>
                  @endif
                </div>
              </div>     

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Bukti Pembayaran</h6>
            </div>
            <div class="card-body">
              <div class="form-group">
                <div class="text-center">
                  <img src="{{ asset('assets/stisla/img/news/img02.jpg') }}" class="rounded" id="image-prev" width="200" alt="avatar">
                </div>
              </div>
              <div class="form-group custom-file mb-3 {{ $errors->has('image') ? ' has-error' : '' }}">
                <input id="image" type="file" class="custom-file-input @if ($errors->has('image')) is-invalid @endif" name="image" />
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                @if ($errors->has('image'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('image') }}</strong>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#image-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#image").change(function() {
      readURL(this);
    });
  </script>
@endsection