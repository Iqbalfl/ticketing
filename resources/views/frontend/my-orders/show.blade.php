@extends('layouts.front')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">

    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Order</h6>
      </div>
      <div class="card-body">
        
        <div class="row">
          <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-header">
                Informasi Order - &nbsp <strong>{{ $order->code }}</strong>
              </div>
              <form method="POST" action="{{ route('my-order.update', $order->id) }}" class="card-body">
                @csrf
                @method('PUT')
                <div>
                  Nama Pembeli<br>
                  <strong>{{ $order->user->name }}</strong>
                </div>
                <hr>
                <div>
                  Tiket <br>
                  <strong>{{ $order->ticket->name }}</strong>
                </div>
                <hr>
                <div>
                  Tanggal Tiket <br>
                  <strong>{{ $order->ticket->event_date }}</strong>
                </div>
                <hr>
                <div>
                  Status Order <br>
                  <span class="badge badge-primary">
                    {{ $order->display_status }}
                  </span>
                </div>
                <hr>
                <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                  <label for="status">Ubah Status</label>
                  <select name="status" class="form-control select2">
                    <option value="100" {{ $order->status == 100 ? 'selected' : '' }}>Menunggu Pembayaran</option>
                    <option value="200" {{ $order->status == 200 ? 'selected' : '' }}>Selesai</option>
                    <option value="10" {{ $order->status == 10 ? 'selected' : '' }}>Dibatalkan</option>
                  </select>
                  @if ($errors->has('status'))
                    <div class="invalid-feedback">
                      {{ $errors->first('status') }}
                    </div>
                  @endif
                </div>

              </form>

            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-primary">
              <div class="card-header">
                Informasi Harga
              </div>
              <div class="card-body">
                <div>
                  Harga Tiket<br>
                  <strong>Rp {{ rupiah($order->ticket->price) }}</strong>
                </div>
                <hr>
                <div>
                  Qty<br>
                  <strong>Rp {{ rupiah($order->quantity) }}</strong>
                </div>
                <hr>
                <div>
                  Total Harga <br>
                  <strong>Rp {{ rupiah($order->total_price) }}</strong>
                </div>
                <hr>
                {{-- <div>
                  Cara Pembayaran <br>
                  <span class="badge badge-primary">
                    <strong>{{ $order->payment_type_name }}</strong>
                  </span>
                </div> 
                <hr> --}}
              </div>
            </div>

          </div>

          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                Histori Pembayaran
              </div>
              <div class="card-body p-0">
                <table class="table table-striped table-md">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Metode Bayar</th>
                      <th>Transfer Atas Nama</th>
                      <th>Jumlah Ditransfer</th>
                      <th>Tanggal Transfer</th>
                      <th>Bukti Pembayaran</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($order->payments as $key => $item)    
                      <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->method }}</td>
                        <td>{{ $item->account_holder }}</td>
                        <td>Rp {{ rupiah($item->total_price) }}</td>
                        <td>{{ date_dmy($item->created_at) }}</td>
                        <td><img class="mr-3 rounded" width="80" src="{{asset('uploads/images/payments/'.$item->image)}}" alt="img"></td>
                        <td><div class="badge badge-info">{{ $item->display_status }}</div></td>
                      </tr>
                    @empty
                      <tr><td colspan="7" class="text-center">Belum Ada Pembayaran</td></tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
            
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection