@extends('layouts.front')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
  <section class="section">
    <div class="card mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Checkout Ticket</h6>
      </div>
      <div class="card-body">
        
        <div class="row">
          <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Informasi Tiket</h6>
              </div>
              <div class="card-body">
              
                <div class="row">
                  <div class="form-group col-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Nama Tiket</label>
                    <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $ticket->name }}" readonly>
                    @if ($errors->has('name'))
                      <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                      </div>
                    @endif
                  </div>
                  <div class="form-group col-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="d-block">Deskripsi</label>
                    <input id="description" type="text" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" value="{{ $ticket->description }}" readonly>
                    @if ($errors->has('description'))
                      <div class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                      </div>
                    @endif
                  </div>
                </div>
  
                <div class="row">
                  <div class="form-group col-12 {{ $errors->has('price') ? ' has-error' : '' }}">
                    <label for="price">Harga Tiket</label>
                    <input id="pricex" type="text" class="form-control @if ($errors->has('price')) is-invalid @endif" name="price" tabindex="1" value="Rp {{ rupiah($ticket->price) }}" readonly>
                    <input id="price" type="hidden" value="{{ $ticket->price }}">
                    @if ($errors->has('price'))
                      <div class="invalid-feedback">
                        {{ $errors->first('price') }}
                      </div>
                    @endif
                  </div>
  
                  <div class="form-group col-12 {{ $errors->has('event_date') ? ' has-error' : '' }}">
                    <label for="event_date">Tanggal Tiket</label>
                    <input id="event_date" type="date" class="form-control @if ($errors->has('event_date')) is-invalid @endif" name="event_date" tabindex="1" value="{{ $ticket->event_date }}" readonly>
                    @if ($errors->has('event_date'))
                      <div class="invalid-feedback">
                        {{ $errors->first('event_date') }}
                      </div>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="video_url" class="d-block">Image / Marker</label>
                  <div class="text-left">
                    @if (is_null($ticket->image))
                      <img src="{{ asset('assets/stisla/img/news/img02.jpg') }}" class="rounded" id="image-prev" width="210" alt="avatar">
                    @else
                      <img alt="image" src="{{asset('uploads/images/tickets/'.$ticket->image)}}" class="rounded" id="image-prev" width="200" alt="images">
                    @endif
                  </div>
                </div>

                <div class="form-group {{ $errors->has('video_url') ? ' has-error' : '' }}">
                  <label for="video_url" class="d-block">Youtube Video</label>
                  <iframe width="500" height="300" src="https://www.youtube.com/embed/{{ getYoutubeIdFromUrl($ticket->video_url) }}" frameborder="0" allowfullscreen></iframe>
                </div>  
  
              </div>
            </div>

          </div>
          <div class="col-md-4">
            <div class="card card-primary">
              <div class="card-header">
                <h6 class="m-0 font-weight-bold">Checkout</h6>
              </div>
              <div class="card-body">
                <form action="{{ route('page.checkout.store') }}" method="POST">
                  <div class="row">
                    <div class="form-group col-12 {{ $errors->has('price') ? ' has-error' : '' }}">
                      <label for="price">Harga Tiket</label>
                      <input id="pricexx" type="text" class="form-control @if ($errors->has('price')) is-invalid @endif" name="price" tabindex="1" value="Rp {{ rupiah($ticket->price) }}" readonly>
                      @if ($errors->has('price'))
                        <div class="invalid-feedback">
                          {{ $errors->first('price') }}
                        </div>
                      @endif
                    </div>
                    <div class="form-group col-12 {{ $errors->has('quantity') ? ' has-error' : '' }}">
                      <label for="quantity" class="d-block">Jumlah Tiket</label>
                      <input id="quantity" type="number" class="form-control @if ($errors->has('quantity')) is-invalid @endif" name="quantity" min="1" value="{{ old('quantity') ?? 1 }}">
                      @if ($errors->has('quantity'))
                        <div class="invalid-feedback">
                          <strong>{{ $errors->first('quantity') }}</strong>
                        </div>
                      @endif
                    </div>
                    <div class="form-group col-12 {{ $errors->has('total_price') ? ' has-error' : '' }}">
                      <label for="total_price" class="d-block">Total Bayar (Rp)</label>
                      <input id="total_price_display" type="number" class="form-control @if ($errors->has('total_price')) is-invalid @endif" name="total_price_display" min="1" value="{{ old('total_price') ?? rupiah($ticket->price) }}" readonly>
                      <input id="total_price" type="hidden" name="total_price" value="{{ $ticket->price }}">
                      @if ($errors->has('total_price'))
                        <div class="invalid-feedback">
                          <strong>{{ $errors->first('total_price') }}</strong>
                        </div>
                      @endif
                    </div>
                  </div>
                  @csrf
                  <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                  <button class="btn btn-success btn-block" {{ $ticket == null ? "disabled" : "" }}>Checkout</button>
                </form>
              </div>
            </div>

            {{-- <div class="alert alert-warning" role="alert">
              <i class="fa fa-info"></i> &nbsp Jika memilih cicilan silahkan membayar Uang Muka sebesar minimal Rp 10.000.000
            </div>

            <div class="alert alert-info" role="alert">
              <i class="fa fa-info"></i> &nbsp Setelah melakukan checkout silahkan melakukan konfirmasi pembayaran di halaman transaksi. Pendaftaran dianggap sah jika telah melakukan transfer dan memberikan bukti pembayaran!
            </div> --}}
            
          </div>
        </div>
            
            
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('assets/stisla/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script>
    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function diffDays(start, end) {
      var a = new Date(start),
      b = new Date(end),
      c = 24*60*60*1000,
      diffDays = Math.round(Math.abs((a - b)/(c)));
      return diffDays;
    }

    $("#quantity").on('keyup', function() {
      var price = $("#price").val();
      var qty = $(this).val();
      var total = price * qty;
      $("#total_price_display").val(total);
      $("#total_price").val(total);
    });

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection