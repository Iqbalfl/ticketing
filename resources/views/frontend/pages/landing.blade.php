@extends('layouts.front')

@section('content')
  {{-- <div class="col-12 mb-4">
    <div class="hero bg-primary text-white">
      <div class="hero-inner">
        <h2>Selamat datang di {{ config('app.name') }}</h2>
        <p class="lead">Solusi penyewaan bus sesama penyedia. Mudah, aman dan murah.</p>
      </div>
    </div>
  </div> --}}
  <section class="section">
    <h2 class="section-title">Tiket Tersedia</h2>
    <div class="row">
      @forelse ($tickets as $item)
        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
          <article class="article article-style-b">
            <div class="article-header">
              <div class="article-image" data-background="{{ asset('uploads/images/tickets/'.$item->image) }}">
              </div>
              {{-- <img alt="image" src="{{asset('uploads/images/tickets/'.$item->image)}}" class="rounded" id="image-prev" width="100%" alt="images"> --}}
            </div>
            <div class="article-details">
              <div class="article-title">
                <h2><a href="javascript:void(0)" onclick="showDetail({{ $item->id }})">{{ $item->name }}</a></h2>
              </div>
              {{-- <strong class="text-danger">Rp {{ rupiah($item->price) }}</strong> <br> --}}
              <p>
                <i class="fa fa-fw fa-dollar-sign"></i> <small>Harga : Rp {{ rupiah($item->price) }}</small> <br>
                <i class="fa fa-fw fa-calendar"></i> <small>Tgl. Tiket : {{ date_dmy($item->date) }}</small> <br>
              </p>
              <div class="article-cta">
                <a href="javascript:void(0)" onclick="showDetail({{ $item->id }})"><i class="fa fa-eye"></i> Detail</a>
              </div>
            </div>
          </article>
        </div>

        <div style="display: none;">
          <input type="hidden" id="ticket-name-{{ $item->id }}" value="{{ $item->name }}">
          <input type="hidden" id="ticket-description-{{ $item->id }}" value="{{ $item->description }}">
          <input type="hidden" id="ticket-price-{{ $item->id }}" value="{{ rupiah($item->price) }}">
          <input type="hidden" id="ticket-date-{{ $item->id }}" value="{{ date_dmy($item->date) }}">
          <input type="hidden" id="ticket-image-{{ $item->id }}" value="{{ $item->image }}">
          <input type="hidden" id="ticket-video_url-{{ $item->id }}" value="https://www.youtube.com/embed/{{ getYoutubeIdFromUrl($item->video_url) }}">
        </div>
      @empty
        <div class="col-md-4 offset-md-5">
          Tidak ada karyawan tersedia :(
        </div>
      @endforelse
      <div class="col-12">
        {{ $tickets->links() }}
      </div>
    </div>
  </section>

    <!-- Modal -->
  <div class="modal fade" tabindex="-1" id="detailModal" role="dialog" aria-labelledby="detailModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detail Tiket</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
          <div class="row">
            <div class="col-md-4">
              <img src="" class="rounded" id="image-ticket" width="220" alt="image">
              <div class="mt-2">
                <strong class="text-primary txt-name">AAA</strong> <br>
                <p>
                  <i class="fa fa-fw fa-dollar-sign"></i> <small>Harga : Rp <span id="ticket-price"></span></small> <br>
                  <i class="fa fa-fw fa-calendar"></i> <small>Tgl. Tiket : <span id="ticket-date"></span></small> <br>
                </p>
              </div>
            </div>
            <div class="col-md-8">
              <strong>Deskripsi</strong>
              <div id="txt-description"></div>

              <div class="mt-3">
                <strong>Video</strong>
                <div id="video-embed"></div>
              </div>
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" type="button" onclick="checkout()" id="btn-a2c" class="btn btn-primary">Pesan</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <form id="booking-form" action="#" method="POST" style="display: none;">
      @csrf
      <input type="hidden" id="ticket-id" name="ticket_id">
  </form>
@endsection

@section('script')
  <script>
    function showDetail(id) {

      var data = {
        name: $(`#ticket-name-${id}`).val(),
        description: $(`#ticket-description-${id}`).val(),
        image: $(`#ticket-image-${id}`).val(),
        price: $(`#ticket-price-${id}`).val(),
        date: $(`#ticket-date-${id}`).val(),
        video_url: $(`#ticket-video_url-${id}`).val(),
      }

      $('.txt-name').html(data.name);
      // $('.txt-rating').html(data.display_rating);
      $('#txt-description').html(data.description);
      $('#ticket-id').val(id);
      $('#ticket-price').text(data.price);
      $('#ticket-date').text(data.date);
      $('#video-embed').html('<iframe id="video-embed" width="450" height="280" src="'+data.video_url+'" frameborder="0" allowfullscreen></iframe>');

      var img = '{{ asset('uploads/images/tickets') }}/' + data.image;
      if (img != null) {
        $('#image-ticket').attr('src', img);
      }

      $('#detailModal').modal('show');
    }

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function checkout() {
      var p_id = $("#ticket-id").val();
      window.location.href = "{{ url('/') }}/checkout/" + p_id;
    }

    @if(Session::has('swal_notification.message'))
      var type = "{{ Session::get('swal_notification.level', 'info') }}";
      switch(type){
        case 'success':
          swal(
            'Sukses!',
            '{{ Session::get('swal_notification.message') }}',
            'success'
          );
          break;

        case 'error':
          swal(
            'Gagal!',
            '{{ Session::get('swal_notification.message') }}',
            'error'
          );
          break;
      }
    @endif
  </script>
@endsection