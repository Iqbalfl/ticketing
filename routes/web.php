<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Frontend\PageController@landing')->name('landing');
Route::get('/about', 'Frontend\PageController@pageAbout')->name('page.about');
// Route::get('/smart-rec', 'Frontend\SmartController@index')->name('smart-rec.index');
// //Route::post('/smart-rec', 'Frontend\SmartController@process')->name('smart-rec.process');
// Route::get('/smart-rec/process', 'Frontend\SmartController@process')->name('smart-rec.process');
// Route::post('/package/detail', 'Frontend\SmartController@detailPackage')->name('pg.detail');
Route::get('/checkout/{id}', 'Frontend\PageController@checkout')->name('page.checkout')->middleware('auth');
Route::post('/checkout', 'Frontend\PageController@checkoutStore')->name('page.checkout.store')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/account/profile', 'ProfileController@show')->name('profile.show');
Route::put('/account/profile', 'ProfileController@update')->name('profile.update');
Route::put('/account/profile/member', 'ProfileController@updateMember')->name('profile.update.member');

Route::group(['prefix' => 'main', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('user', 'Backend\UserController@index' )->name('user.index');
    Route::post('user', 'Backend\UserController@store' )->name('user.store');
    Route::get('user/create', 'Backend\UserController@create' )->name('user.create');
    Route::get('user/{id}/edit', 'Backend\UserController@edit' )->name('user.edit');
    Route::put('user/{id}/update', 'Backend\UserController@update' )->name('user.update');
    Route::delete('user/{id}', 'Backend\UserController@destroy' )->name('user.destroy');

    Route::resource('ticket', 'Backend\TicketController');
    Route::resource('order', 'Backend\OrderController');
    Route::resource('payment', 'Backend\PaymentController');
});


Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'role:user']], function () {
    Route::get('/my-order/{id}/cancel', 'Frontend\MyOrderController@cancel')->name('my-order.cancel');
    Route::resource('my-order', 'Frontend\MyOrderController');
});